package dsn

import (
	"testing"
)

type test struct {
	input string
	want  DSN
}

var rabbitMQDsn = []test{
	{input: "amqp://host", want: DSN{Scheme: "amqp", Host: "host", Port: 5672, Database: ""}},
	{input: "amqp://user@host", want: DSN{Scheme: "amqp", Username: "user", Host: "host", Port: 5672, Database: ""}},
	{input: "amqp://user:pass@host", want: DSN{Scheme: "amqp", Username: "user", Password: "pass", Host: "host", Port: 5672, Database: ""}},
	{input: "amqp://user:pass@host/", want: DSN{Scheme: "amqp", Username: "user", Password: "pass", Host: "host", Port: 5672, Database: ""}},
	{input: "amqp://user:pass@host/vhost", want: DSN{Scheme: "amqp", Username: "user", Password: "pass", Host: "host", Port: 5672, Database: "vhost"}},
	{input: "amqp://user:pass@host:5555/vhost", want: DSN{Scheme: "amqp", Username: "user", Password: "pass", Host: "host", Port: 5555, Database: "vhost"}},
	{input: "amqp://user:pass@host:5672/vhost", want: DSN{Scheme: "amqp", Username: "user", Password: "pass", Host: "host", Port: 5672, Database: "vhost"}},
}

var postgreSQLDsn = []test{
	{input: "postgresql://host", want: DSN{Scheme: "pgsql", Host: "host", Port: 5432, Database: "postgres"}},
	{input: "pgsql://user@host", want: DSN{Scheme: "pgsql", Username: "user", Host: "host", Port: 5432, Database: "postgres"}},
	{input: "pgsql://user:pass@host", want: DSN{Scheme: "pgsql", Username: "user", Password: "pass", Host: "host", Port: 5432, Database: "postgres"}},
	{input: "pgsql://user:pass@host/", want: DSN{Scheme: "pgsql", Username: "user", Password: "pass", Host: "host", Port: 5432, Database: "postgres"}},
	{input: "pgsql://user:pass@host/db", want: DSN{Scheme: "pgsql", Username: "user", Password: "pass", Host: "host", Port: 5432, Database: "db"}},
	{input: "pgsql://user:pass@host:5555/db", want: DSN{Scheme: "pgsql", Username: "user", Password: "pass", Host: "host", Port: 5555, Database: "db"}},
	{input: "pgsql://user:pass@host:5432/db", want: DSN{Scheme: "pgsql", Username: "user", Password: "pass", Host: "host", Port: 5432, Database: "db"}},
}

var redisDsn = []test{
	{input: "redis://host", want: DSN{Scheme: "redis", Host: "host", Port: 6379, Database: ""}},
	{input: "redis://user@host", want: DSN{Scheme: "redis", Password: "user", Host: "host", Port: 6379, Database: ""}},
	{input: "redis://:pass@host", want: DSN{Scheme: "redis", Password: "pass", Host: "host", Port: 6379, Database: ""}},
	{input: "redis://user:pass@host", want: DSN{Scheme: "redis", Username: "user", Password: "pass", Host: "host", Port: 6379, Database: ""}},
	{input: "redis://user:pass@host/", want: DSN{Scheme: "redis", Username: "user", Password: "pass", Host: "host", Port: 6379, Database: ""}},
	{input: "redis://user:pass@host/20", want: DSN{Scheme: "redis", Username: "user", Password: "pass", Host: "host", Port: 6379, Database: "20"}},
	{input: "redis://user:pass@host:5555/20", want: DSN{Scheme: "redis", Username: "user", Password: "pass", Host: "host", Port: 5555, Database: "20"}},
	{input: "redis://user:pass@host:6379/20", want: DSN{Scheme: "redis", Username: "user", Password: "pass", Host: "host", Port: 6379, Database: "20"}},
}

//redis://[pass@][ip|host|socket[:port]][/db-index]

func TestDsnRabbitMQ(t *testing.T) {

	for _, dsnTest := range rabbitMQDsn {
		dsnOut, _ := Parse(dsnTest.input)
		if dsnOut.Scheme != dsnTest.want.Scheme ||
			dsnOut.Username != dsnTest.want.Username ||
			dsnOut.Password != dsnTest.want.Password ||
			dsnOut.Host != dsnTest.want.Host ||
			dsnOut.Port != dsnTest.want.Port ||
			dsnOut.Database != dsnTest.want.Database {
			t.Errorf("Parse() got %v, want %v)", dsnOut, dsnTest.want)
		}
	}
}

func TestDsnPostgreSQL(t *testing.T) {

	for _, dsnTest := range postgreSQLDsn {
		dsnOut, _ := Parse(dsnTest.input)
		if dsnOut.Scheme != dsnTest.want.Scheme ||
			dsnOut.Username != dsnTest.want.Username ||
			dsnOut.Password != dsnTest.want.Password ||
			dsnOut.Host != dsnTest.want.Host ||
			dsnOut.Port != dsnTest.want.Port ||
			dsnOut.Database != dsnTest.want.Database {
			t.Errorf("Parse() got %v, want %v)", dsnOut, dsnTest.want)
		}
	}
}

func TestDsnRedis(t *testing.T) {

	for _, dsnTest := range redisDsn {
		dsnOut, _ := Parse(dsnTest.input)
		if dsnOut.Scheme != dsnTest.want.Scheme ||
			dsnOut.Username != dsnTest.want.Username ||
			dsnOut.Password != dsnTest.want.Password ||
			dsnOut.Host != dsnTest.want.Host ||
			dsnOut.Port != dsnTest.want.Port ||
			dsnOut.Database != dsnTest.want.Database {
			t.Errorf("Parse() got %v, want %v)", dsnOut, dsnTest.want)
		}
	}
}
