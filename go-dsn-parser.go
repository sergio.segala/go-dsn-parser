package dsn

import (
	"log"
	"net/url"
	"os"
	"strconv"
	"strings"
)

type DSN struct {
	Scheme   string
	Username string            // username information
	Password string            // password information
	Host     string            // host or host:port
	Port     int               // service port
	Database string            // path (relative paths may omit leading slash)
	Params   map[string]string // query string
	Fragment string            // fragment for references, without '#'
}

func Parse(urlstr string) (*DSN, error) {
	u, err := url.Parse(urlstr)
	if err != nil {
		return nil, err
	}

	pw, _ := u.User.Password()
	result := &DSN{
		Scheme:   u.Scheme,
		Username: u.User.Username(),
		Password: pw,
		Host:     u.Host,
		Database: u.Path,
		Fragment: u.Fragment}

	if u.Scheme == "" {
		result.Scheme = "file"
	}
	// Split query
	entries := strings.Split(u.RawQuery, "&")
	query := make(map[string]string)
	for _, e := range entries {
		if len(e) > 0 {
			parts := strings.Split(e, "=")
			if len(parts) == 1 {
				query[parts[0]] = parts[0]
			} else {
				query[parts[0]] = parts[1]
			}
		}
	}
	result.Params = query
	if i := strings.IndexRune(result.Host, ':'); i != -1 {
		result.Port, err = strconv.Atoi(result.Host[i+1 : len(result.Host)])
		if err != nil {
			log.Println(err)
			os.Exit(2) // panic here
		}
		result.Host = result.Host[:i]
	}

	purge(result)

	return result, nil
}

func purgeDatabaseName(url *DSN) {
	if i := strings.IndexRune(url.Database, '/'); i == 0 {
		url.Database = url.Database[1:len(url.Database)]
	}
}
func purgeDatabasePort(url *DSN, defaultPort int) {
	if url.Port == 0 {
		url.Port = defaultPort
	}
}

func purge(url *DSN) error {
	switch url.Scheme {
	case "pgsql", "postgres", "postgresql":
		url.Scheme = "pgsql"
		purgeDatabaseName(url)
		if url.Database == "" {
			url.Database = "postgres"
		}
		purgeDatabasePort(url, 5432)
	case "amqp":
		purgeDatabaseName(url)
		purgeDatabasePort(url, 5672)
	case "redis":
		purgeDatabaseName(url)
		if url.Username != "" && url.Password == "" {
			url.Password = url.Username
			url.Username = ""
		}
		purgeDatabasePort(url, 6379)
	}
	return nil
}
